
#!/bin/bash

# download needed addtional assets for using windows bindings
rustup target add x86_64-pc-windows-msvc
cargo install xwin

if [ -d .xwin ];
then
    echo "skipping xwin install as it already exists."
else
    xwin --accept-license splat --output .xwin
fi 
apt update
apt install lld -y

mkdir .cargo 

echo '
[target.x86_64-pc-windows-msvc]
linker = "lld"
rustflags = [
  "-Lnative=.xwin/crt/lib/x86_64",
  "-Lnative=.xwin/sdk/lib/um/x86_64",
  "-Lnative=.xwin/sdk/lib/ucrt/x86_64"
]' > .cargo/config.toml


cargo build --target=x86_64-pc-windows-msvc
cp target/x86_64-pc-windows-msvc/debug/plantcare.exe plantcare.exe

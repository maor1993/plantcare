use crate::components::{
    self,
    common::{load_spritesheet_by_name, SoundHashMap, SpriteHashmap, TextureAtlasGrid},
};
use bevy::{prelude::*, utils::HashMap};
use bevy_kira_audio::prelude::*;

#[cfg(target_os = "windows")]
pub fn load_all_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    components::common::load_spritesheet(
        &mut commands,
        &asset_server,
        &mut texture_atlases,
        "sprites/enemies",
        TextureAtlasGrid::enemies_default(),
        components::enemy::EnemySprites,
    );
    components::common::load_spritesheet(
        &mut commands,
        &asset_server,
        &mut texture_atlases,
        "sprites/plants",
        TextureAtlasGrid::plants_default(),
        components::plant::PlantSprites,
    );
    components::common::load_spritesheet(
        &mut commands,
        &asset_server,
        &mut texture_atlases,
        "sprites/player",
        TextureAtlasGrid::player_default(),
        components::player::PlayerSprites,
    );
    components::common::load_spritesheet(
        &mut commands,
        &asset_server,
        &mut texture_atlases,
        "sprites/tap",
        TextureAtlasGrid::tap_default(),
        components::tap::TapSprites,
    );
    components::common::load_sounds(
        &mut commands,
        &asset_server,
        "sounds/enemy",
        components::enemy::EnemySounds,
    );
    components::common::load_sounds(
        &mut commands,
        &asset_server,
        "sounds/tap",
        components::tap::TapSounds,
    );
    components::common::load_sounds(
        &mut commands,
        &asset_server,
        "sounds/plant",
        components::plant::PlantSounds,
    );
}

// as bevy wasm can't load folder we will have to manually list all assets for loading.
#[cfg(target_arch = "wasm32")]
const PLANTSPRITESLIST: [&str; 4] = [
    "sprites/plants/avocado.png",
    "sprites/plants/coleus.png",
    "sprites/plants/monstera2.png",
    "sprites/plants/succulent1.png",
];
#[cfg(target_arch = "wasm32")]
const ENEMIESPRITESLIST: [&str; 2] = ["sprites/enemies/kimhit.png", "sprites/enemies/wurm.png"];

#[cfg(target_arch = "wasm32")]
pub fn load_all_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    /*--------sprites---------*/

    //player
    let mut hashmap = SpriteHashmap(HashMap::new());
    let handle = load_spritesheet_by_name(
        &asset_server,
        &mut texture_atlases,
        "sprites/player/player.png",
        TextureAtlasGrid::player_default(),
    );
    hashmap.0.insert(0, handle);
    commands
        .spawn(hashmap)
        .insert(components::player::PlayerSprites);

    //tap
    let mut hashmap = SpriteHashmap(HashMap::new());
    let handle = load_spritesheet_by_name(
        &asset_server,
        &mut texture_atlases,
        "sprites/tap/tap.png",
        TextureAtlasGrid::tap_default(),
    );
    hashmap.0.insert(0, handle);
    commands.spawn(hashmap).insert(components::tap::TapSprites);

    //plants
    let mut hashmap = SpriteHashmap(HashMap::new());
    for (idx, path) in PLANTSPRITESLIST.iter().enumerate() {
        let handle = load_spritesheet_by_name(
            &asset_server,
            &mut texture_atlases,
            path,
            TextureAtlasGrid::plants_default(),
        );
        hashmap.0.insert(idx, handle);
    }
    commands
        .spawn(hashmap)
        .insert(components::plant::PlantSprites);

    //enemies
    let mut hashmap = SpriteHashmap(HashMap::new());
    for (idx, path) in ENEMIESPRITESLIST.iter().enumerate() {
        let handle = load_spritesheet_by_name(
            &asset_server,
            &mut texture_atlases,
            path,
            TextureAtlasGrid::enemies_default(),
        );
        hashmap.0.insert(idx, handle);
    }
    commands
        .spawn(hashmap)
        .insert(components::enemy::EnemySprites);
    /*------------------------------------------ */

    /*-------------------sounds------------------*/
    let mut hashmap = SoundHashMap(HashMap::new());
    let handle: Handle<AudioSource> = asset_server.load("sounds/enemy/crunch-noise.wav");
    hashmap.0.insert(0, handle);
    commands
        .spawn(hashmap)
        .insert(components::enemy::EnemySounds);

    let mut hashmap = SoundHashMap(HashMap::new());
    let handle: Handle<AudioSource> = asset_server.load("sounds/tap/water-drop-8.wav");
    hashmap.0.insert(0, handle);
    commands.spawn(hashmap).insert(components::tap::TapSounds);

    let mut hashmap = SoundHashMap(HashMap::new());
    let handle: Handle<AudioSource> = asset_server.load("sounds/plant/scissor-snip.wav");
    hashmap.0.insert(0, handle);
    commands
        .spawn(hashmap)
        .insert(components::plant::PlantSounds);
}

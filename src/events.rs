use crate::{components::plant::PlantTypes, ui::MenuSelection};
use bevy::prelude::*;

//location of event structs, actual handling and registration occurs per implmentor

pub struct WaterFillSoundEvent;

pub struct EnemyCrunchSoundEvent;

pub struct PlantPropigationSoundEvent;
pub struct PlantSeedlingEvent {
    pub seedling: PlantTypes,
    pub pos: Vec2,
}

pub struct PlantPropigationEvent {
    pub seed: PlantTypes,
}

pub struct PlantDamageEvent{
    pub damage: u32,
    pub plantpos: Vec2
}


pub struct MenuSelectionEvent{
    pub selection: MenuSelection
}
#![windows_subsystem = "windows"]
use bevy::app::AppExit;
use bevy::prelude::*;
use bevy::time::FixedTimestep;
use bevy_kira_audio::prelude::*;
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::prelude::*;

mod components;
use components::common::*;
use components::*;
mod events;
mod gameconsts;
mod loader;
mod platform;
mod ui;
use events::{MenuSelectionEvent, PlantSeedlingEvent};
use gameconsts::*;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Copy)]
enum GameState {
    MAINMENU,
    PLAYING,
    GAMEOVER,
}

#[derive(Component)]
struct Enemycnt(usize);

#[derive(Component)]
struct Plantcnt(usize);
#[derive(Component)]
struct Score(usize);

#[derive(Component)]
struct ScoreText;

#[derive(Resource)]
struct ScoreTimer(Timer);

#[derive(Bundle)]
struct GameMain {
    score: Score,
}

fn update_game_state(
    mut state: ResMut<State<GameState>>,
    q2: Query<&plant::Plant>,
    q3: Query<&player::Inventory, With<player::Player>>,
    evplant: EventReader<PlantSeedlingEvent>,
    mut events: EventReader<MenuSelectionEvent>,
    mut exit: EventWriter<AppExit>,
) {
    let plants_alive = q2.iter().count();
    match state.current() {
        GameState::PLAYING => {
            let seedcnt = q3
                .single()
                .items
                .iter()
                .filter(|ic| ic.variant_eq(&player::Items::Seed(default())))
                .count();

            if (plants_alive == 0) & (seedcnt == 0) & (evplant.is_empty()) {
                state
                    .set(GameState::GAMEOVER)
                    .expect("Failed To set game state")
            }
        }
        GameState::GAMEOVER => {
            for _ev in events.iter() {
                state.set(GameState::MAINMENU).unwrap();
            }
        }
        GameState::MAINMENU => {
            for ev in events.iter() {
                match ev.selection {
                    ui::MenuSelection::PLAY => state.set(GameState::PLAYING).unwrap(),
                    ui::MenuSelection::QUIT => exit.send(AppExit),
                }
            }
        }
    }
}

fn update_score(
    time: Res<Time>,
    mut timer: ResMut<ScoreTimer>,
    state: Res<State<GameState>>,
    mut query: Query<&mut Score>,
    mut q2: Query<&mut Text, With<ScoreText>>,
    q3: Query<(&plant::PlantData, &plant::PlantState), With<plant::Plant>>,
) {
    let mut score = query.single_mut();

    if timer.0.tick(time.delta()).just_finished() {
        //update score with all good plants
        let addscore = q3.iter().fold(0, |x: usize, (data, state)| {
            x + if *state == plant::PlantState::OK {
                data.get_score_multiplier() as usize
            } else {
                0
            }
        });
        score.0 += addscore;

        match state.current() {
            GameState::MAINMENU => {
                score.0 = 0;
            }
            GameState::PLAYING => {
                let mut text = q2.single_mut();
                text.sections[1].value = format!("{}", score.0);
            }
            _ => (),
        }
    }
}

fn setup(
    mut cmd: Commands,
    asset_server: Res<AssetServer>,
    mut rapier_config: ResMut<RapierConfiguration>,
) {
    rapier_config.gravity = Vec2::new(0.0, -520.0);
    let background_image = asset_server.load("sprites/skybox/Daylight_Box.png");

    cmd.spawn(Camera2dBundle::default());
    cmd.spawn(SpriteBundle {
        texture: background_image,
        transform: Transform::from_xyz(0.0, 0.0, -0.05),
        ..Default::default()
    });
    cmd.spawn(GameMain { score: Score(0) });
}

struct ElementsPlugin;
impl Plugin for ElementsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(platform::PlatformPlugin)
            .add_plugin(enemy::EnemyPlugin)
            .add_plugin(plant::PlantPlugin)
            .add_plugin(HpPlugin)
            .add_plugin(WaterPlugin)
            .add_plugin(player::PlayerPlugin)
            .add_plugin(tap::TapPlugin);
    }
}

fn main() {
    App::new()
        .insert_resource(Msaa { samples: 4 })
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "PlantCare".to_string(),
                width: 800.,
                height: 600.,
                resizable: false,
                ..default()
            },
            ..default()
        }))
        .add_plugin(ShapePlugin)
        .add_state(GameState::MAINMENU)
        .insert_resource(ScoreTimer(Timer::from_seconds(1.0, TimerMode::Repeating)))
        .add_startup_system(setup)
        .add_startup_system_to_stage(StartupStage::PreStartup, loader::load_all_assets)
        .add_plugin(ElementsPlugin)
        .add_system(update_score)
        .add_system(debug_damage)
        .add_plugin(ui::MenuPlugin)
        .add_system(update_game_state)
        .add_plugin(AudioPlugin)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(
            PIXELS_PER_METER,
        ))
        // .add_plugin(RapierDebugRenderPlugin::default())
        .run();
}

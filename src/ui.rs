use crate::events::MenuSelectionEvent;
use crate::gameconsts::MAIN_FONT_PATH;
use crate::{GameState, Score, ScoreText};
use bevy::prelude::*;

#[derive(Clone, Copy, Debug)]
pub enum MenuSelection {
    PLAY,
    QUIT,
}

#[derive(Component)]
struct UIItem;

#[derive(Component)]
struct Selected(bool, MenuSelection);

fn despawn_ui_elements(mut commands: Commands, query: Query<Entity, With<UIItem>>) {
    for ent in query.iter() {
        commands.entity(ent).despawn_recursive();
    }
}

fn spawn_game_over(mut commands: Commands, asset_server: Res<AssetServer>, score: Query<&Score>) {
    let font: Handle<Font> = asset_server.load(MAIN_FONT_PATH);

    let text_style = TextStyle {
        font,
        font_size: 60.0,
        color: Color::WHITE,
    };
    let text_alignment = TextAlignment::CENTER;

    commands.spawn((Text2dBundle {
        text: Text::from_section(
            format!("GAMEOVER\nScore:{}", score.single().0),
            text_style.clone(),
        )
        .with_alignment(text_alignment),
        ..Default::default()
    },UIItem));
}

fn handle_click_on_gameover(
    state: Res<State<GameState>>,
    mut keys: ResMut<Input<KeyCode>>,
    mut ev: EventWriter<MenuSelectionEvent>,
) {
    if (*state.current() == GameState::GAMEOVER) & (keys.just_pressed(KeyCode::Space)) {
        ev.send(MenuSelectionEvent {
            selection: MenuSelection::PLAY,
        });
        keys.reset(KeyCode::Space);
    }
    
}

fn spawn_game_ui(mut commands: Commands, asset_server: Res<AssetServer>) {
    let font: Handle<Font> = asset_server.load(MAIN_FONT_PATH);

    commands.spawn((
        // Create a TextBundle that has a Text with a list of sections.
        TextBundle::from_sections([
            TextSection::new(
                "Score:",
                TextStyle {
                    font: font.clone(),
                    font_size: 60.0,
                    color: Color::WHITE,
                },
            ),
            TextSection::from_style(TextStyle {
                font: font.clone(),
                font_size: 60.0,
                color: Color::GOLD,
            }),
        ]),
        ScoreText,
        UIItem,
    ));
}

fn spawn_main_menu(mut commands: Commands, asset_server: Res<AssetServer>) {
    let font: Handle<Font> = asset_server.load(MAIN_FONT_PATH);
    let gamesign = asset_server.load("sprites/menu/Title.png");

    commands
        .spawn(ButtonBundle {
            style: Style {
                align_content: AlignContent::Center,
                align_self: AlignSelf::Center,
                justify_content: JustifyContent::Center,
                margin: UiRect::all(Val::Auto),
                size: Size {
                    width: Val::Percent(25.0),
                    height: Val::Percent(5.0),
                },
                ..default()
            },
            background_color: BackgroundColor(Color::BLACK),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle {
                text: Text::from_section(
                    "Start Game",
                    TextStyle {
                        font: font.clone(),
                        font_size: 25.0,
                        color: Color::rgb(0.9, 0.9, 0.9),
                    },
                ),
                ..Default::default()
            });
        })
        .insert(UIItem)
        .insert(Selected(true, MenuSelection::PLAY));
    commands
        .spawn(ButtonBundle {
            style: Style {
                align_content: AlignContent::Center,
                align_self: AlignSelf::Center,
                justify_content: JustifyContent::Center,
                margin: UiRect::all(Val::Auto),
                size: Size {
                    width: Val::Percent(25.0),
                    height: Val::Percent(5.0),
                },
                ..default()
            },
            background_color: BackgroundColor(Color::DARK_GRAY),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle {
                text: Text::from_section(
                    "Quit",
                    TextStyle {
                        font: font.clone(),
                        font_size: 25.0,
                        color: Color::rgb(0.9, 0.9, 0.9),
                    },
                ),
                ..Default::default()
            });
        })
        .insert(UIItem)
        .insert(Selected(false, MenuSelection::QUIT));

        commands.spawn(SpriteBundle{
            texture:gamesign,
            transform: Transform::from_xyz(0.0,150.0,0.0),
            ..default()
        }).insert(UIItem);
}

fn handle_menu_controls(
    mut items: Query<(&mut BackgroundColor, &mut Selected), With<Button>>,
    state: Res<State<GameState>>,
    mut keys: ResMut<Input<KeyCode>>,
    mut ev: EventWriter<MenuSelectionEvent>,
) {
    if *state.current() == GameState::MAINMENU {
        for (mut color, mut selected) in items.iter_mut() {
            if keys.just_pressed(KeyCode::Right) | keys.just_pressed(KeyCode::Left) {
                selected.0 = !selected.0;
                color.0 = if selected.0 {
                    Color::BLACK
                } else {
                    Color::DARK_GRAY
                };
            }

            if keys.just_pressed(KeyCode::Space) {
                if selected.0 {
                    ev.send(MenuSelectionEvent {
                        selection: selected.1.clone(),
                    });
                }
            }
        }
        keys.reset(KeyCode::Right);
        keys.reset(KeyCode::Left);
        keys.reset(KeyCode::Space);
    }
}

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(GameState::MAINMENU).with_system(spawn_main_menu))
            .add_system_set(
                SystemSet::on_exit(GameState::MAINMENU).with_system(despawn_ui_elements),
            )
            .add_system_set(SystemSet::on_enter(GameState::PLAYING).with_system(spawn_game_ui))
            .add_system_set(
                SystemSet::on_enter(GameState::GAMEOVER)
                    .with_system(despawn_ui_elements)
                    .with_system(spawn_game_over),
            )
            .add_system_set(
                SystemSet::on_exit(GameState::GAMEOVER).with_system(despawn_ui_elements),
            )
            .add_event::<MenuSelectionEvent>()
            .add_system(handle_menu_controls)
            .add_system(handle_click_on_gameover);
    }
}

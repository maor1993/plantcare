use super::common::playsound;
use crate::components::common as cmn;
use crate::{events::*, GameState};
use bevy::prelude::*;

const PLOP_SOUND_IDX: usize = 0;

#[derive(Component, Default)]
pub struct Tap;

#[derive(Bundle, Default)]
pub struct TapBundle {
    _e: Tap,

    #[bundle]
    sprite: SpriteSheetBundle,
}

#[derive(Component)]
pub struct TapSprites;

#[derive(Component)]
pub struct TapSounds;

fn spawn_tap(
    mut commands: Commands,
    tapsprites: Query<&cmn::SpriteHashmap, With<TapSprites>>,
    windows: Res<Windows>,
) {
    let win = windows.get_primary().unwrap();
    let (h, w) = (win.height(), win.width());
    let sprite = SpriteSheetBundle {
        sprite: TextureAtlasSprite::new(0),

        texture_atlas: tapsprites.single().0.get(&0).unwrap().clone(),
        transform: Transform::from_xyz(w / 2.0 - 10.0, -h / 2.0 + 70.0, 0.0),
        ..default()
    };

    commands.spawn(TapBundle {
        sprite: sprite,
        ..default()
    });
}

fn despawn_tap(mut commands: Commands, query: Query<Entity, With<Tap>>) {
    commands.entity(query.single()).despawn_recursive();
}

pub struct TapPlugin;

impl Plugin for TapPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(GameState::PLAYING).with_system(spawn_tap))
            .add_system_set(SystemSet::on_enter(GameState::GAMEOVER).with_system(despawn_tap))
            .add_system(playsound::<WaterFillSoundEvent, TapSounds, PLOP_SOUND_IDX>);
    }
}

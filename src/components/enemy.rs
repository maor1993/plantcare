use crate::GameState;
//enemy.rs
//main module which defines the behivour of a enemy
use crate::components::common::*;
use crate::events::*;
use crate::gameconsts::*;
use bevy::prelude::*;
use bevy::time::FixedTimestep;
use bevy_rapier2d::prelude::*;
use rand::seq::IteratorRandom;
use rand::Rng;

use crate::{ENEMY_STRAVTAION_DAMAGE, TIME_STEP};

use super::plant::Plant;
// region: CONSTS
const KIMHIT_SPRITE_IDX: usize = 0;
const WURM_SPRITE_IDX: usize = 1;
const ENEMY_CRUNCH_IDX: usize = 0;
// endregion

#[derive(Component)]
pub struct EnemyName(pub String);
#[derive(Component)]
pub struct EnemyData {
    pub dps: u32,
    movespeed: f32,
}

#[derive(Component)]
pub struct Enemy;
#[derive(Component)]
pub struct EnemySprites;

#[derive(Component)]
pub struct EnemySounds;

#[derive(Bundle)]
pub struct EnemyBundle {
    pub hp: Health,
    pub name: EnemyName,
    pub data: EnemyData,
    pub _e: Enemy,
    #[bundle]
    sprite: SpriteSheetBundle,
}
#[derive(Copy, Clone)]
pub enum EnemyTypes {
    _NOTHING,
    KIMHIT,
    WURM,
}

impl EnemyBundle {
    pub fn get_enemy(kind: EnemyTypes, spirte: SpriteSheetBundle) -> Option<EnemyBundle> {
        match kind {
            EnemyTypes::KIMHIT => Some(EnemyBundle {
                hp: Health::new(3),
                name: EnemyName("Kimhit".to_string()),
                data: EnemyData {
                    dps: 1,
                    movespeed: 60.0,
                },
                _e: Enemy,
                sprite: spirte,
            }),
            EnemyTypes::WURM => Some(EnemyBundle {
                hp: Health::new(6),
                name: EnemyName("Wurm".to_string()),
                data: EnemyData {
                    dps: 1,
                    movespeed: 90.0,
                },
                _e: Enemy,
                sprite: spirte,
            }),
            _ => None,
        }
    }
}

#[derive(Component, Deref, DerefMut)]
struct AnimationTimer(Timer);

fn animate_sprite(
    time: Res<Time>,
    texture_atlases: Res<Assets<TextureAtlas>>,
    mut query: Query<(
        &mut AnimationTimer,
        &mut TextureAtlasSprite,
        &Handle<TextureAtlas>,
    )>,
) {
    for (mut timer, mut sprite, texture_atlas_handle) in &mut query {
        timer.tick(time.delta());
        if timer.just_finished() {
            let texture_atlas = texture_atlases.get(texture_atlas_handle).unwrap();
            sprite.index = (sprite.index + 1) % texture_atlas.textures.len();
        }
    }
}

fn create_rand_enemy(
    mut commands: Commands,
    enemysprites: Query<&SpriteHashmap, With<EnemySprites>>,
    plants: Query<&Transform, (Without<Enemy>, With<Plant>)>,
    query: Query<&Enemy>,
) {
    let enemysprites = enemysprites.single();
    let enemies = [EnemyTypes::KIMHIT, EnemyTypes::WURM];
    let mut rng = rand::thread_rng();

    if query.iter().count() >= crate::MAX_ENEMIES {
        return;
    }
    let enemy = enemies.iter().choose(&mut rng).unwrap();

    let tas = TextureAtlasSprite::new(0);

    let handle = match enemy {
        EnemyTypes::KIMHIT => enemysprites.0.get(&KIMHIT_SPRITE_IDX).unwrap(),
        EnemyTypes::WURM => enemysprites.0.get(&WURM_SPRITE_IDX).unwrap(),
        _ => return,
    };

    let plant: Option<&Transform> = plants.iter().choose(&mut rng);

    let (x, y) = match plant {
        None => return, //don't attempt to spawn until platforms have been created.
        Some(t) => {
            let xoffset = if t.translation.x > 0.0 { -30.0 } else { 30.0 };
            (t.translation.x + xoffset, t.translation.y)
        }
    };

    let sprite = SpriteSheetBundle {
        sprite: tas,
        texture_atlas: handle.clone(),
        transform: Transform::from_xyz(x, y, 1.0),
        ..default()
    };
    let i = EnemyBundle::get_enemy(*enemy, sprite).expect("failed to create enemy!");
    let hpbar = i.hp.draw(&mut commands);
    let enemy = commands
        .spawn((
            i,
            AnimationTimer(Timer::from_seconds(0.1, TimerMode::Repeating)),
        ))
        .insert(Collider::cuboid(12.0, 12.0))
        .insert(RigidBody::Dynamic)
        .insert(Restitution::coefficient(0.2))
        .insert(LockedAxes::ROTATION_LOCKED)
        .id();

    commands.entity(enemy).add_child(hpbar);
}

fn move_enemies(
    mut query: Query<(&mut Transform, &EnemyData), With<Enemy>>,
    q2: Query<&Transform, (With<Plant>, Without<Enemy>)>,
) {
    for (mut enemypos, enemydata) in query.iter_mut() {
        // max distance
        let max_distance = TIME_STEP * enemydata.movespeed;
        let random: f32 = rand::thread_rng().gen_range(0.0..max_distance);

        let plantpos =
            find_closest_from_entities::<Plant, Enemy>(enemypos.translation.truncate(), &q2);

        let dir = match (enemypos.translation.x - plantpos.x) < 0.0 {
            true => 1.0,
            false => -1.0,
        };

        enemypos.translation.x += random * dir;
    }
}

fn handle_plant_attack(
    mut query: Query<(&Transform, &mut Health, &EnemyData), With<Enemy>>,
    plants: Query<&Transform, (With<Plant>, Without<Enemy>)>,
    mut attackevent: EventWriter<PlantDamageEvent>,
    mut crunchevent: EventWriter<EnemyCrunchSoundEvent>,
) {
    for (enemypos, mut enemyhp, enemydata) in query.iter_mut() {
        let enemyvec = enemypos.translation.truncate();
        let plantpos = find_closest_from_entities(enemyvec, &plants);

        enemyhp.hp = match ((enemyvec - plantpos).length() > HITBOX_RADIUS, enemyhp.hp) {
            (true, None) => None,
            (true, Some(x)) if x > 0 => Some(x - ENEMY_STRAVTAION_DAMAGE),
            (true, Some(_x)) => None,
            (false, _) => {
                crunchevent.send(EnemyCrunchSoundEvent);
                attackevent.send(PlantDamageEvent {
                    damage: enemydata.dps,
                    plantpos,
                });
                enemyhp.hp
            }
        }
    }
}

fn despawn_enemies(mut commands: Commands, query: Query<Entity, With<Enemy>>) {
    for ent in query.iter() {
        commands.entity(ent).despawn_recursive();
    }
}

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(5.0))
                .with_system(create_rand_enemy),
        )
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(1.0))
                .with_system(handle_plant_attack),
        )
        .add_system_set(SystemSet::on_enter(GameState::GAMEOVER).with_system(despawn_enemies))
        .add_event::<EnemyCrunchSoundEvent>()
        .add_system(playsound::<EnemyCrunchSoundEvent, EnemySounds, ENEMY_CRUNCH_IDX>)
        .add_system(animate_sprite)
        .add_system(move_enemies);
    }
}

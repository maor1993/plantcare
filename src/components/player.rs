use crate::components::common::*;
use crate::components::*;
use crate::events::*;
use crate::gameconsts::*;
use crate::GameState;
use bevy::prelude::*;
use bevy::time::FixedTimestep;
use bevy_rapier2d::prelude::*;

use super::common::WaterLevel;
use super::plant::{Plant, PlantTypes};

const PLAYER_SPEED: f32 = 3.0;
const PLAYER_WATERING_DISTANCE: f32 = 30.0; // in pixels, will most likely change one we have upgrades...
const WATERINGCAN_MAX: usize = 10;

#[derive(Default, Debug, Component, PartialEq, Eq, PartialOrd, Ord)]
pub enum Items {
    #[default]
    None,
    WateringCan(usize),
    PestSpray(usize),
    PottingCompound,
    Seed(PlantTypes),
}

impl Items {
    pub fn variant_eq(&self, other: &Items) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}

#[derive(Component, Default)]
pub struct Inventory {
    pub items: Vec<Items>,
}

//inital inventory has a watering can which has WATERINGCAN_MAX waterings
impl Inventory {
    fn new() -> Inventory {
        Inventory {
            items: vec![
                Items::WateringCan(WATERINGCAN_MAX),
                Items::Seed(PlantTypes::MONSTERA),
                Items::Seed(PlantTypes::SUCCULENT),
                Items::Seed(PlantTypes::AVOCADO),
                Items::Seed(PlantTypes::COLEUS),
            ],
        }
    }

    fn get_mut_watercan(&mut self) -> Option<&mut Items> {
        return self
            .items
            .iter_mut()
            .find(|x| x.variant_eq(&Items::WateringCan(default())));
    }
}

#[derive(Component, Default)]
pub struct JumpCounter(usize);

#[derive(Component, Default)]
pub struct Player;

#[derive(Component)]
pub struct PlayerSprites;

#[derive(Bundle, Default)]
pub struct PlayerBundle {
    pub _e: Player,
    pub inv: Inventory,
    pub jumps: JumpCounter,
    #[bundle]
    sprite: SpriteSheetBundle,
}

fn water_plants(
    mut playerinfo: Query<(&Transform, &mut Inventory), With<Player>>,
    mut plants: Query<(&Transform, &mut WaterLevel), With<plant::Plant>>,
    mut keys: ResMut<Input<KeyCode>>,
) {
    let (playerpos, mut playerinv) = match playerinfo.get_single_mut() {
        Ok(x) => x,
        Err(_) => return,
    };

    let watered = keys.just_pressed(KeyCode::Z);
    let wateringcan = playerinv.get_mut_watercan().unwrap();
    for (pos, mut thirst) in plants.iter_mut() {
        let playerclose =
            (pos.translation - playerpos.translation).length() <= PLAYER_WATERING_DISTANCE;

        let waterlevel = if let Items::WateringCan(ref mut x) = wateringcan {
            x
        } else {
            panic!("Don't know why I'm here")
        };

        let enoughwater = waterlevel.to_owned() > 0;
        keys.reset(KeyCode::Z);
        match (watered, enoughwater, playerclose) {
            (true, true, true) => {
                thirst.level += 10.0;
                *waterlevel -= 1;
            }
            (true, true, false) => {
                *waterlevel -= 1;
            }
            _ => (),
        }
    }
}

fn plant_seed(
    mut playerinfo: Query<(&Transform, &Velocity, &mut Inventory), With<Player>>,
    mut event: EventWriter<PlantSeedlingEvent>,
    qplant: Query<&Transform, (With<Plant>, Without<Player>)>,
    keys: Res<Input<KeyCode>>,
) {
    let (playerpos, playerspeed, mut playerinv) = match playerinfo.get_single_mut() {
        Ok(x) => x,
        Err(_) => return,
    };
    let playerxy = playerpos.translation.truncate();
    let playerisstatic = playerspeed.linvel.y <= f32::EPSILON;
    let playerisnotclosetoplant = (playerxy - find_closest_from_entities(playerxy, &qplant))
        .length()
        > PLAYER_WATERING_DISTANCE * 2.0;
    let pressed_plant_key = keys.just_pressed(KeyCode::X);
    let maxplants = qplant.iter().count() <= MAX_PLANTS;

    if pressed_plant_key & playerisstatic & playerisnotclosetoplant & maxplants {
        let seed = playerinv
            .items
            .iter()
            .enumerate()
            .find(|x| x.1.variant_eq(&Items::Seed(default())));

        //TODO: avoid allowing more plants if max plants has reached
        let (idx, planttype) = if let Some((k, Items::Seed(x))) = seed {
            (k, x)
        } else {
            //player has no seeds
            return;
        };
        event.send(PlantSeedlingEvent {
            seedling: planttype.clone(),
            pos: playerxy,
        });
        //despawn seed
        playerinv.items.remove(idx);
    }
}

fn get_seed(mut event: EventReader<PlantPropigationEvent>, mut query: Query<&mut Inventory>) {
    let mut playerinv = match query.get_single_mut() {
        Ok(x) => x,
        Err(_) => return,
    };

    for ev in event.iter() {
        playerinv.items.push(Items::Seed(ev.seed));
    }
}

fn fill_can(
    mut playerinfo: Query<(&Transform, &mut Inventory), With<Player>>,
    mut my_events: EventWriter<WaterFillSoundEvent>,
    taploc: Query<&Transform, With<tap::Tap>>,
) {
    let (playerpos, mut playerinv) = match playerinfo.get_single_mut() {
        Ok(x) => x,
        Err(_) => return,
    };
    let taploc = taploc.single();
    let wateringcan = playerinv.get_mut_watercan().unwrap();

    let waterlevel = if let Items::WateringCan(ref mut x) = wateringcan {
        x
    } else {
        panic!("Don't know why I'm here")
    };

    let watercannotfull = *waterlevel != WATERINGCAN_MAX;
    let playerclose =
        (taploc.translation - playerpos.translation).length() <= PLAYER_WATERING_DISTANCE;

    if watercannotfull && playerclose {
        *waterlevel = WATERINGCAN_MAX;
        my_events.send(WaterFillSoundEvent);
    }
}

fn move_player(
    mut query: Query<(&mut Transform, &mut Velocity, &mut JumpCounter), With<Player>>,
    mut playersprite: Query<&mut TextureAtlasSprite, With<Player>>,
    keys: Res<Input<KeyCode>>,
) {
    let (mut pos, mut playerspeed, mut jumpcnt) = match query.get_single_mut() {
        Ok(x) => x,
        Err(_) => return, //player hasn't spawned yet.
    };

    let mut playery_movement = 0.0;

    //reset condition for doublejump bool
    if playerspeed.linvel.length() <= f32::EPSILON {
        jumpcnt.0 = 0;
    }

    if keys.pressed(KeyCode::Left) {
        pos.translation.x -= PLAYER_SPEED;
        playersprite.single_mut().flip_x = false;
    }
    if keys.pressed(KeyCode::Right) {
        pos.translation.x += PLAYER_SPEED;
        playersprite.single_mut().flip_x = true;
    }
    if (keys.just_pressed(KeyCode::Space)) && (jumpcnt.0 < 2) {
        playery_movement += 300.0;
        jumpcnt.0 += 1;
    }
    playerspeed.linvel += Vec2 {
        x: 0.0,
        y: playery_movement,
    };
}

fn spawn_player(mut commands: Commands, query: Query<&SpriteHashmap, With<PlayerSprites>>) {
    let sprites = query.single();

    let tas = TextureAtlasSprite::new(0);
    let handle = sprites.0.get(&0).unwrap();

    let sprite = SpriteSheetBundle {
        sprite: tas,
        texture_atlas: handle.clone(),
        transform: Transform::from_xyz(0.0, -100.0, 0.0),
        ..default()
    };
    let player = PlayerBundle {
        sprite: sprite,
        inv: Inventory::new(),
        ..Default::default()
    };

    //create the bundle
    commands
        .spawn(player)
        .insert(Collider::cuboid(16.0, 16.0))
        .insert(Velocity {
            ..Default::default()
        })
        .insert(RigidBody::Dynamic)
        .insert(Restitution::coefficient(0.2))
        .insert(LockedAxes::ROTATION_LOCKED);
}
fn despawn_player(mut commands: Commands, query: Query<Entity, With<Player>>) {
    commands.entity(query.single()).despawn_recursive();
}

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(SystemSet::on_enter(GameState::PLAYING).after("Platform").with_system(spawn_player))
            .add_system_set(SystemSet::on_enter(GameState::GAMEOVER).with_system(despawn_player))
            .add_system(move_player)
            .add_system(water_plants)
            .add_system(fill_can)
            .add_system(plant_seed)
            .add_system(get_seed)
            .add_event::<WaterFillSoundEvent>();
    }
}

use bevy::ecs::event::Event;
//common.rs
// componeants which are common along different components
use bevy::prelude::*;
use bevy::time::FixedTimestep;
use bevy::utils::HashMap;
use bevy_inspector_egui::Inspectable;
use bevy_kira_audio::prelude::*;
use bevy_prototype_lyon::prelude::*;

#[derive(Component, Default, Inspectable)]
pub struct Health {
    pub hp: Option<u32>,
    pub max: u32,
}

#[derive(Component)]
pub struct HealthSprite;

impl Health {
    pub fn new(hp: u32) -> Self {
        Health {
            hp: Some(hp),
            max: hp,
        }
    }

    pub fn draw(&self, commands: &mut Commands) -> Entity {
        let shape = shapes::Line {
            0: Vec2 { y: 30., x: -16. },
            1: Vec2 {
                y: 30.,
                x: (self.hp.unwrap() * 50 / self.max) as f32 - 16.,
            },
        };
        commands
            .spawn((
                GeometryBuilder::build_as(
                    &shape,
                    DrawMode::Outlined {
                        fill_mode: FillMode::color(Color::LIME_GREEN),
                        outline_mode: StrokeMode::new(Color::LIME_GREEN, 3.0),
                    },
                    Transform::from_xyz(0., 0., 0.),
                ),
                HealthSprite,
            ))
            .id()
    }

    pub fn update(
        q_parent: Query<&Health>,
        mut q_child: Query<(&Parent, &mut Path, &mut DrawMode), With<HealthSprite>>,
    ) {
        for (parent, mut path, mut draw_mode) in q_child.iter_mut() {
            let hp = match q_parent.get(parent.get()) {
                Ok(x) => x,
                Err(_) => continue,
            };

            if hp.hp == None {
                continue; // enttiy is dead, should be disposed soon.
            }

            let polygon = shapes::Line {
                0: Vec2 { y: 30., x: -16. },
                1: Vec2 {
                    y: 30.,
                    x: (hp.hp.unwrap() * 50 / hp.max) as f32 - 16.,
                },
            };

            let color = match hp.hp.unwrap() * 100 / hp.max {
                0..=39 => Color::RED,
                40..=60 => Color::YELLOW,
                _ => Color::LIME_GREEN,
            };

            if let DrawMode::Outlined {
                ref mut fill_mode,
                ref mut outline_mode,
            } = *draw_mode
            {
                fill_mode.color = color;
                outline_mode.color = color;
            }

            *path = ShapePath::build_as(&polygon);
        }
    }
    fn remove_dead_entites(mut commands: Commands, query: Query<(Entity, &Health)>) {
        //clear all old sprites
        for (ent, hp) in query.iter() {
            if hp.hp == None {
                commands.entity(ent).despawn_recursive();
            }
        }
    }
}

pub struct HpPlugin;

impl Plugin for HpPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(Health::update).add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(0.1))
                .with_system(Health::remove_dead_entites),
        );
    }
}

#[derive(Component)]
pub struct ThirstSprite;

#[derive(Component, Inspectable)]
pub struct WaterLevel {
    pub level: f32,
    pub limits: (f32, f32),
}

impl Default for WaterLevel {
    fn default() -> Self {
        WaterLevel {
            level: 50.0,
            limits: (0.0, 0.0),
        }
    }
}

impl WaterLevel {
    pub fn draw(&self, commands: &mut Commands) -> Entity {
        let shape = shapes::Line {
            0: Vec2 { y: 35., x: -16. },
            1: Vec2 {
                y: 35.,
                x: self.level - 16.,
            },
        };
        commands
            .spawn((
                GeometryBuilder::build_as(
                    &shape,
                    DrawMode::Outlined {
                        fill_mode: FillMode::color(Color::AQUAMARINE),
                        outline_mode: StrokeMode::new(Color::AQUAMARINE, 3.0),
                    },
                    Transform::from_xyz(0., 0., 0.),
                ),
                ThirstSprite,
            ))
            .id()
    }
    pub fn update(
        q_parent: Query<&WaterLevel>,
        mut q_child: Query<(&Parent, &mut Path, &mut DrawMode), With<ThirstSprite>>,
    ) {
        for (parent, mut path, mut draw_mode) in q_child.iter_mut() {
            let water = match q_parent.get(parent.get()) {
                Ok(x) => x,
                Err(_) => continue,
            };

            let polygon = shapes::Line {
                0: Vec2 { y: 35., x: -16. },
                1: Vec2 {
                    y: 35.,
                    x: water.level - 16.,
                },
            };
            let color = match (water.limits.0..=water.limits.1).contains(&water.level) {
                true => Color::AQUAMARINE,
                false => Color::RED,
            };

            if let DrawMode::Outlined {
                ref mut fill_mode,
                ref mut outline_mode,
            } = *draw_mode
            {
                fill_mode.color = color;
                outline_mode.color = color;
            }

            *path = ShapePath::build_as(&polygon);
        }
    }
}

pub struct WaterPlugin;

impl Plugin for WaterPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(WaterLevel::update);
    }
}

pub struct TextureAtlasGrid {
    spritesize: Vec2, //TODO: convert to struct
    columns: usize,
    rows: usize,
}
impl TextureAtlasGrid {
    pub fn plants_default() -> Self {
        TextureAtlasGrid {
            spritesize: Vec2 { x: 64.0, y: 64.0 },
            columns: 2,
            rows: 2,
        }
    }
    pub fn player_default() -> Self {
        TextureAtlasGrid {
            spritesize: Vec2 { x: 32.0, y: 32.0 },
            columns: 2,
            rows: 2,
        }
    }
    pub fn enemies_default() -> Self {
        TextureAtlasGrid {
            spritesize: Vec2 { x: 24.0, y: 24.0 },
            columns: 4,
            rows: 1,
        }
    }
    pub fn tap_default() -> Self {
        TextureAtlasGrid {
            spritesize: Vec2 { x: 32.0, y: 32.0 },
            columns: 1,
            rows: 1,
        }
    }
}

#[derive(Component)]
pub struct SpriteHashmap(pub HashMap<usize, Handle<TextureAtlas>>);

pub fn load_spritesheet(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
    spritepath: &str,
    grid: TextureAtlasGrid,
    owner: impl Component,
) {
    let assets = asset_server
        .load_folder(spritepath)
        .expect("Failed to load assets");

    let mut sprites = SpriteHashmap(HashMap::new());
    for (idx, asset) in assets.into_iter().enumerate() {
        let handle: Handle<Image> = asset.typed();
        let texture_atlas =
            TextureAtlas::from_grid(handle, grid.spritesize, grid.columns, grid.rows, None, None);
        let texture_atlas_handle = texture_atlases.add(texture_atlas);
        sprites.0.insert(idx, texture_atlas_handle);
    }
    commands.spawn(sprites).insert(owner);
}

#[derive(Component)]
pub struct SoundHashMap(pub HashMap<usize, Handle<AudioSource>>);
pub fn load_sounds(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    soundpath: &str,
    owner: impl Component,
) {
    let assets = asset_server
        .load_folder(soundpath)
        .expect("Failed to load assets");
    let mut sounds = SoundHashMap(HashMap::new());
    for (idx, asset) in assets.into_iter().enumerate() {
        let handle: Handle<AudioSource> = asset.typed();
        sounds.0.insert(idx, handle);
    }
    commands.spawn(sounds).insert(owner);
}

pub fn find_closest_from_entities<T: Component, K: Component>(
    yourpos: Vec2,
    query: &Query<&Transform, (With<T>, Without<K>)>,
) -> Vec2 {
    let mut prevclosest = f32::MAX;
    query.iter().fold(
        Vec2 {
            x: f32::MAX,
            y: f32::MAX,
        },
        |acc: Vec2, it| {
            let objpos = it.translation.truncate();
            let dist = (yourpos - objpos).length();
            if dist < prevclosest {
                prevclosest = dist;
                objpos
            } else {
                acc
            }
        },
    )
}

pub fn playsound<E: Event, T: Component, const K: usize>(
    mut events: EventReader<E>,
    query: Query<&SoundHashMap, With<T>>,
    audio: Res<Audio>,
) {
    let map = query.single();
    for _my_event in events.iter() {
        audio.play(map.0.get(&K).unwrap().to_owned());
    }
}

pub fn debug_damage(mut query: Query<(&mut TextureAtlasSprite, ChangeTrackers<Health>)>) {
    for (mut sprite, tracker) in query.iter_mut() {
        // detect if the Health changed this frame
        if tracker.is_changed() {
            sprite.color = Color::RED;
        } else {
            // extra check so we don't mutate on every frame without changes
            if sprite.color == Color::RED {
                sprite.color = Color::WHITE;
            }
        }
    }
}

pub fn load_spritesheet_by_name(
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
    spritepath: &str,
    grid: TextureAtlasGrid,
) -> Handle<TextureAtlas> {
    let handle: Handle<Image> = asset_server.load(spritepath);
    texture_atlases.add(TextureAtlas::from_grid(
        handle,
        grid.spritesize,
        grid.columns,
        grid.rows,
        None,
        None,
    ))
}

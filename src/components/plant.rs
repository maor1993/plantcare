//plant.rs
//main module which defines the behivour of a plant

use crate::components::common::*;
use crate::events::*;
use crate::gameconsts::PLANT_WATER_DAMAGE;
use crate::platform::*;
use bevy::prelude::*;

use bevy::time::FixedTimestep;
use bevy_inspector_egui::prelude::*;
use bevy_rapier2d::prelude::*;
use rand::seq::IteratorRandom;

use super::player::Player;

const LIFETICK: f32 = 0.2;
const PLANT_SNIP_IDX: usize = 0;

#[derive(Clone, Copy, Component, Default, Inspectable, PartialEq)]
pub enum PlantAge {
    #[default]
    SPROUT = 0,
    PLANT = 1,
    MATURE = 2,
    FULL = 3,
}

#[derive(Component, Default, Inspectable)]
pub struct PlantLifeCnt(f32);

#[derive(Component, Default, PartialEq, Eq, Inspectable)]
pub enum PlantState {
    #[default]
    OK,
    DEHYDRATED,
    OVERFLOWN,
    SICK,
}

#[derive(Component, Default)]
pub struct PlantName(pub String);

#[derive(Component, Default, Inspectable)]
pub struct PlantData {
    rarity: i32,
    thirstrate: f32,
    growthrate: f32,
}

#[derive(Component, Default, Inspectable)]
pub struct Plant;

#[derive(Component)]
pub struct PlantSprites;

#[derive(Component, Debug, Clone, Copy, Default, Inspectable, PartialEq, Eq, PartialOrd, Ord)]
pub enum PlantTypes {
    #[default]
    AVOCADO = 0,
    COLEUS = 1,
    MONSTERA = 2,
    SUCCULENT = 3,
    // FOTUS = 2,
}

#[derive(Bundle, Default)]
pub struct PlantBundle {
    pub hp: Health,
    pub thirst: WaterLevel,
    pub state: PlantState,
    pub age: PlantAge,
    pub lifecnt: PlantLifeCnt,
    pub name: PlantTypes,
    pub data: PlantData,
    pub _e: Plant,
    glowtimer: GlowTimer,
    #[bundle]
    sprite: SpriteSheetBundle,
}

#[derive(Component)]
pub struct PlantSounds;

impl PlantData {
    pub fn get_score_multiplier(&self) -> f32 {
        self.growthrate * (self.rarity as f32)
    }
}

impl PlantBundle {
    pub fn get_plant(kind: PlantTypes, spirte: SpriteSheetBundle) -> Option<PlantBundle> {
        match kind {
            PlantTypes::COLEUS => Some(PlantBundle {
                hp: Health::new(8),
                name: PlantTypes::COLEUS,
                data: PlantData {
                    rarity: 1,
                    thirstrate: 1.0,
                    growthrate: 3.0,
                },
                thirst: WaterLevel {
                    limits: (20.0, 70.0),
                    ..default()
                },
                sprite: spirte,
                ..Default::default()
            }), 
            PlantTypes::AVOCADO => Some(PlantBundle {
                hp: Health::new(12),
                name: PlantTypes::AVOCADO,
                data: PlantData {
                    rarity: 1,
                    thirstrate: 1.0,
                    growthrate: 4.0,
                },
                thirst: WaterLevel {
                    limits: (15.0, 60.0),
                    ..default()
                },
                sprite: spirte,
                ..Default::default()
            }),
            PlantTypes::MONSTERA => Some(PlantBundle {
                hp: Health::new(8),
                name: PlantTypes::MONSTERA,
                data: PlantData {
                    rarity: 2,
                    thirstrate: 1.0,
                    growthrate: 4.0,
                },
                thirst: WaterLevel {
                    limits: (15.0, 60.0),
                    ..default()
                },
                sprite: spirte,
                ..Default::default()
            }),
            PlantTypes::SUCCULENT => Some(PlantBundle {
                hp: Health::new(10),
                name: PlantTypes::SUCCULENT,
                data: PlantData {
                    rarity: 1,
                    thirstrate: 0.5,
                    growthrate: 3.0,
                },
                thirst: WaterLevel {
                    limits: (10.0, 50.0),
                    ..default()
                },
                sprite: spirte,
                ..Default::default()
            }),
            _ => None,
        }
    }
}

fn update_lifecnt_plant(mut query: Query<(&PlantState, &mut PlantLifeCnt), With<Plant>>) {
    for (plantstate, mut lifecnt) in query.iter_mut() {
        lifecnt.0 = match *plantstate {
            PlantState::OK => lifecnt.0 + LIFETICK,
            _ => lifecnt.0,
        }
    }
}

fn update_age_plant(
    mut query: Query<
        (
            &mut TextureAtlasSprite,
            &mut PlantAge,
            &PlantData,
            &mut PlantLifeCnt,
        ),
        With<Plant>,
    >,
) {
    for (mut sprite, mut age, data, lifecnt) in query.iter_mut() {
        let stage1 = (0.0..=data.growthrate).contains(&lifecnt.0);
        let stage2 = (data.growthrate..=data.growthrate * 2.0).contains(&lifecnt.0);
        let stage3 = (data.growthrate * 2.0..=data.growthrate * 3.0).contains(&lifecnt.0);
        let stage4 = (data.growthrate * 3.0..).contains(&lifecnt.0);
        *age = match (*age, stage1, stage2, stage3, stage4) {
            (PlantAge::SPROUT, false, true, false, false) => PlantAge::PLANT,
            (PlantAge::PLANT, false, false, true, false) => PlantAge::MATURE,
            (PlantAge::MATURE, false, false, false, true) => PlantAge::FULL,
            (PlantAge::FULL, _, _, _, false) => PlantAge::PLANT, //handles propigation
            _ => *age,
        };
        if *age as usize != sprite.index {
            sprite.index = *age as usize;
        }
    }
}

//assuming position given is on an actual platform
fn create_plant_from_event(
    mut commands: Commands,
    plantsprites: Query<&SpriteHashmap, With<PlantSprites>>,
    query: Query<&Plant>,
    mut events: EventReader<PlantSeedlingEvent>,
) {
    let spirtes = plantsprites.single();

    if query.iter().count() >= crate::MAX_PLANTS {
        return;
    }

    for ev in events.iter() {
        let tas = TextureAtlasSprite::new(0);

        let handle = spirtes.0.get(&(ev.seedling as usize)).unwrap();
        let sprite = SpriteSheetBundle {
            sprite: tas,
            texture_atlas: handle.clone(),
            transform: Transform::from_xyz(ev.pos.x, ev.pos.y + 16.0, 0.0),
            ..default()
        };
        let i = PlantBundle::get_plant(ev.seedling, sprite).expect("failed to create enemy!");
        let hpbar = i.hp.draw(&mut commands);
        let waterbar = i.thirst.draw(&mut commands);
        let plant = commands.spawn(i).id();
        commands.entity(plant).add_child(hpbar).add_child(waterbar);
    }
}

fn give_player_seedling(
    mut query: Query<
        (&Transform, &PlantTypes, &PlantData, &mut PlantLifeCnt),
        (With<Plant>, Without<Player>),
    >,
    qplayer: Query<&Transform, (With<Player>, Without<Plant>)>,
    mut event: EventWriter<PlantPropigationEvent>,
    mut sound: EventWriter<PlantPropigationSoundEvent>,
) {
    let playerpos = match qplayer.get_single() {
        Ok(x) => x,
        Err(_) => return,
    };

    for (pos, kind, data, mut life) in query.iter_mut() {
        let plantisready = life.0 >= (data.growthrate * 3.0);

        if ((playerpos.translation - pos.translation).length() < 20.0) & plantisready {
            // player is close enough to this plant and its ready to be propigated, give the player a seedling
            event.send(PlantPropigationEvent { seed: kind.clone() });
            sound.send(PlantPropigationSoundEvent);
            //lower the lifecnt to transform the plant back to a smaller state
            life.0 = data.growthrate + f32::EPSILON;

            info!("Sent Plant");
        }
    }
}

fn _create_rand_plant(
    platforms: Query<(&Transform, &Collider), With<Platform>>,
    mut event: EventWriter<PlantSeedlingEvent>,
    query: Query<&Plant>,
) {
    let plants = [PlantTypes::MONSTERA; 3];
    let mut rng = rand::thread_rng();

    if query.iter().count() >= crate::MAX_PLANTS {
        return;
    }

    for plant in plants {
        let platform: Option<(&Transform, &Collider)> = platforms.iter().choose(&mut rng);

        let (x, y) = match platform {
            None => return, //don't attempt to spawn until platforms have been created.
            Some((t, c)) => (
                t.translation.x,
                t.translation.y + c.as_cuboid().unwrap().half_extents().y,
            ),
        };

        event.send(PlantSeedlingEvent {
            seedling: plant,
            pos: Vec2 { x, y },
        })
    }
}

fn thirst_tick(
    mut query: Query<(&mut WaterLevel, &PlantData, &mut PlantState, &Transform)>,
    mut event: EventWriter<PlantDamageEvent>,
) {
    for (mut thirst, data, mut state, plantpos) in query.iter_mut() {
        thirst.level = f32::max(0.0, thirst.level - data.thirstrate);

        let (minthirst, maxthirst) = thirst.limits;
        *state = match (thirst.level >= minthirst, thirst.level <= maxthirst) {
            (true, true) => PlantState::OK,
            (false, true) => PlantState::DEHYDRATED,
            (true, false) => PlantState::OVERFLOWN,
            (false, false) => panic!("how is thirst both too high and too low???"),
        };

        if *state != PlantState::OK {
            event.send(PlantDamageEvent {
                damage: PLANT_WATER_DAMAGE,
                plantpos: plantpos.translation.truncate(),
            })
        }
    }
}

#[derive(Component, Deref, DerefMut)]
struct GlowTimer(Timer);

impl Default for GlowTimer {
    fn default() -> Self {
        GlowTimer(Timer::from_seconds(0.4, TimerMode::Repeating))
    }
}

fn animate_propigation(
    time: Res<Time>,
    mut query: Query<(&mut GlowTimer, &mut TextureAtlasSprite, &PlantAge)>,
) {
    for (mut timer, mut sprite, plantage) in query.iter_mut() {
        let ready = *plantage == PlantAge::FULL;
        let iswhite = sprite.color == Color::WHITE;
        let isgold = sprite.color == Color::GOLD;
        timer.0.tick(time.delta());
        sprite.color = match (ready, timer.0.just_finished(), iswhite, isgold) {
            (_, true, false, true) => Color::WHITE,
            (true, true, true, false) => Color::GOLD,
            _ => sprite.color,
        };
    }
}

fn calculate_plant_damage(
    mut damgeevents: EventReader<PlantDamageEvent>,
    mut query: Query<(&Transform, &mut Health), With<Plant>>,
) {
    for ev in damgeevents.iter() {
        for (plantpos, mut planthp) in query.iter_mut() {
            let mut damage = 0;
            if ev.plantpos == plantpos.translation.truncate() {
                damage += ev.damage;
            }

            match (planthp.hp, damage) {
                (_, 0) => (),
                (Some(x), y) if y < x => planthp.hp = Some(x - y),
                (Some(_x), _y) => planthp.hp = None,
                (None, _) => (),
            }
        }
    }
}

pub struct PlantPlugin;

impl Plugin for PlantPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(1.0))
                .with_system(thirst_tick)
                .with_system(update_lifecnt_plant),
        )
        .add_event::<PlantSeedlingEvent>()
        .add_event::<PlantPropigationEvent>()
        .add_event::<PlantPropigationSoundEvent>()
        .add_event::<PlantDamageEvent>()
        .add_system(calculate_plant_damage)
        .add_system(playsound::<PlantPropigationSoundEvent, PlantSounds, PLANT_SNIP_IDX>)
        .add_system(create_plant_from_event)
        .add_system(give_player_seedling)
        .add_system(animate_propigation)
        .add_system(update_age_plant);
    }
}

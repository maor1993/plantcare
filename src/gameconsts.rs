//game constants
pub const TIME_STEP: f32 = 1. / 60.;
pub const MAX_ENEMIES: usize = 3; //TODO: needs to change vs diffculty selected
pub const MAX_PLANTS: usize = 10; //TODO: also an upgrade?
pub const HITBOX_RADIUS: f32 = 30.0;
pub const PLANT_WATER_DAMAGE: u32 = 1; //TODO: needs to change vs diffculty selected
pub const ENEMY_STRAVTAION_DAMAGE: u32 = 1;
pub const MAIN_FONT_PATH: &str = "fonts/Roboto-Regular.ttf";
pub const PIXELS_PER_METER: f32 = 492.3;

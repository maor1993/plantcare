//platform.rs
//builds the game platforms

use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use rand::seq::IteratorRandom;

use crate::GameState;

const PLATFORMS: usize = 5;
const PLATFROMSIZE: (f32, f32) = (80.0, 10.0);
const PLATFORMRADIUS: usize = 3;

#[derive(Component)]
pub struct Platform;

//TODO: handle start game
fn create_rand_platforms(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    windows: Res<Windows>,
) {
    let win = windows.get_primary().unwrap();
    let (h, w) = (win.height(), win.width());
    let mut rng = rand::thread_rng();
    let plattexture = asset_server.load("textures/wooddeck.png");
    let mut locs: Vec<Vec2> = Vec::new();

    let mut lastx = 0;
    let mut lasty = 0;
    for _i in 0..PLATFORMS {
        let x = (lastx..=lastx + (PLATFROMSIZE.0 as usize) * PLATFORMRADIUS * 2)
            .step_by((PLATFROMSIZE.0 * 2.0) as usize)
            .choose(&mut rng)
            .unwrap();
        let y = (lasty..=lasty + (PLATFROMSIZE.1 as usize) * PLATFORMRADIUS * 2)
            .step_by((PLATFROMSIZE.1 * 2.0) as usize)
            .choose(&mut rng)
            .unwrap();

        lastx = x % (w as usize);
        lasty = y % (h as usize);

        locs.push(Vec2 {
            x: lastx as f32 - w / 2.0,
            y: lasty as f32 - h / 4.0 - h / 10.0,
        });
    }
    locs.sort_by(|a, b| a.y.total_cmp(&b.y));
    locs.sort_by(|a, b| a.x.total_cmp(&b.x));

    let mut locs2 = locs.clone();

    //when to drop?
    // if same x and y
    // if on the x,  y is 2 platform heights above /below another platform (remember platform size is half size)
    for loc in locs.windows(2) {
        if (loc[0] == loc[1])
            || (((loc[0].y - loc[1].y).abs() <= PLATFROMSIZE.1 * 4.0) && (loc[0].x == loc[1].x))
        {
            let dropped = match locs2.binary_search_by(|curr| curr.x.total_cmp(&loc[0].x)) {
                Ok(x) => locs2.remove(x),
                Err(_x) => Vec2 { x: 0.0, y: 0.0 },
            };
            debug!("Dropping {}", dropped);
        }
    }

    for loc in locs2 {
        debug!("making platform at: {}", loc);
        commands
            .spawn(SpriteBundle {
                sprite: Sprite {
                    rect: Some(Rect {
                        max: Vec2 {
                            x: PLATFROMSIZE.0*2.0,
                            y: PLATFROMSIZE.1*2.0,
                        },
                        ..Default::default()
                    }),
                    ..default()
                },
                texture: plattexture.clone(),
                transform: Transform::from_translation(loc.extend(0.0)),
                ..default()
            })
            .insert(Collider::cuboid(PLATFROMSIZE.0, PLATFROMSIZE.1))
            .insert(Platform);
    }
}

//TODO: handle start game
fn build_main_platform(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    windows: Res<Windows>,
) {
    let win = windows.get_primary().unwrap();
    let (h, _w) = (win.height(), win.width());
    let groundtexture = asset_server.load("textures/dirt.png");
    let (platx, platy) = (1000.0, 100.0);
    /* Create the ground. */
    commands
        .spawn(SpriteBundle {
            sprite: Sprite {
                rect: Some(Rect {
                    max: Vec2 { x: platx, y: platy },
                    ..Default::default()
                }),
                ..default()
            },
            texture: groundtexture,
            transform: Transform {
                translation: Vec3 {
                    x: 0.0,
                    y: -h / 2.0,
                    z: 0.0,
                },
                ..default()
            },

            ..Default::default()
        })
        .insert(Collider::cuboid(platx / 2.0, platy / 2.0))
        .insert(Platform);
}

fn create_bounds(mut commands: Commands, windows: Res<Windows>) {
    let win = windows.get_primary().unwrap();
    let (h, w) = (win.height(), win.width());

    //right wall
    commands
        .spawn(TransformBundle::from(Transform::from_xyz(
            w / 2.0,
            0.0,
            0.0,
        )))
        .insert(Collider::cuboid(1.0, h))
        .insert(Platform);

    //left wall
    commands
        .spawn(TransformBundle::from(Transform::from_xyz(
            -w / 2.0,
            0.0,
            0.0,
        )))
        .insert(Collider::cuboid(1.0, h))
        .insert(Platform);

    //ceiling
    commands
        .spawn(TransformBundle::from(Transform::from_xyz(
            0.0,
            h / 2.0,
            0.0,
        )))
        .insert(Collider::cuboid(w, 1.0))
        .insert(Platform);
}

fn despawn_platforms(mut commands: Commands, query: Query<Entity, With<Platform>>) {
    for ent in query.iter() {
        commands.entity(ent).despawn_recursive();
    }
}

pub struct PlatformPlugin;

impl Plugin for PlatformPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_enter(GameState::PLAYING)
                .with_system(build_main_platform)
                .with_system(create_bounds)
                .with_system(create_rand_platforms)
                .label("Platform"),
        )
        .add_system_set(SystemSet::on_enter(GameState::GAMEOVER).with_system(despawn_platforms));
    }
}
